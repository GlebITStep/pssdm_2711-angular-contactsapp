import { Injectable, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Contact } from '../models/contact';
import { ContactsApiService } from './contacts-api.service';

@Injectable({
  providedIn: 'root'
})
export class ContactsStorageService {

  contacts = new BehaviorSubject<Array<Contact>>([]);
  // public contacts$ = this.contacts.asObservable();

  constructor(private contactsApi: ContactsApiService) {
    this.init();
  }

  async init() {
    this.contacts.next(await this.contactsApi.get());
  }

  create(contact: Contact) {
    this.contacts.next([...this.contacts.getValue(), contact]);
  }

  remove(id: number) {
    this.contacts.next(this.contacts.getValue().filter(x => x.id != id));
  }
}


// let newContacts = this.contacts.getValue();
// newContacts.push(contact);
// this.contacts.next(newContacts);



// //spread operator
// let arr1 = [1,2,3];
// let arr2 = [9, 8];
// let arr3 = [...arr1, 4, 5, ...arr2]; //1 2 3 4 5 9 8
