import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Contact } from '../models/contact';

@Injectable({
  providedIn: 'root'
})
export class ContactsApiService {
  readonly apiUrl = 'http://localhost:52692';

  constructor(private httpClient: HttpClient) { }

  get(): Promise<Array<Contact>> {
    return this.httpClient.get<Array<Contact>>(`${this.apiUrl}/api/contacts`).toPromise();
  }

  create(contact: Contact): Promise<Contact> {
    return this.httpClient.post<Contact>(`${this.apiUrl}/api/contacts`, contact).toPromise();
  }

  remove(id: number) {
    return this.httpClient.delete(`${this.apiUrl}/api/contacts/${id}`).toPromise();
  }
}
