import { Component, OnInit } from '@angular/core';
import { Contact } from 'src/app/models/contact';
import { ContactsStorageService } from 'src/app/services/contacts-storage.service';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss'],
})
export class ContactListComponent implements OnInit {
  // contacts = new Array<Contact>();

  constructor(
    public contactsStorage: ContactsStorageService) {}

  async ngOnInit() {
    // this.contactsStorage.contacts.subscribe(x => {
    //   this.contacts = x;
    //   console.log(x); 
    // });
  }

  // async updateContacts(id: number) {
  //   console.log(id); 
  //   this.contacts = await this.contactsApi.get();
  // }
}
