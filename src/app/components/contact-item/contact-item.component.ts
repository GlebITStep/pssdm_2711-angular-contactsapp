import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Contact } from 'src/app/models/contact';
import { ContactsApiService } from 'src/app/services/contacts-api.service';
import { ContactsStorageService } from 'src/app/services/contacts-storage.service';

@Component({
  selector: 'app-contact-item',
  templateUrl: './contact-item.component.html',
  styleUrls: ['./contact-item.component.scss']
})
export class ContactItemComponent implements OnInit {
  @Input() contact: Contact;
  // @Output() deleted = new EventEmitter<number>();

  constructor(
    private contactsStorage: ContactsStorageService,
    private contactsApi: ContactsApiService) { }

  ngOnInit() {
  }

  async onDelete() {
    try {
      await this.contactsApi.remove(this.contact.id);  
      this.contactsStorage.remove(this.contact.id);
    } catch (error) {
      console.log(error);  
    }
    // this.deleted.emit(this.contact.id);
  }
}
