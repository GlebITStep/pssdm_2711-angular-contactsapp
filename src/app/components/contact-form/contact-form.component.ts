import { Component, OnInit } from '@angular/core';
import { ContactsApiService } from 'src/app/services/contacts-api.service';
import { Contact } from 'src/app/models/contact';
import { ContactsStorageService } from 'src/app/services/contacts-storage.service';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss'],
})
export class ContactFormComponent implements OnInit {
  newContact: Contact = {};

  constructor(
    private contactsStorage: ContactsStorageService,
    private contactsApi: ContactsApiService) {}

  ngOnInit() {
  }

  async onSubmit() {
    try {
      let contact = await this.contactsApi.create(this.newContact);
      this.contactsStorage.create(contact);      
      this.newContact = {};
    } catch (error) {
      console.log(error); 
    }
  }

}
